package com.simpsonwil.bukkit.blquests.parts;

/**
 * Created by wil on 6/20/2015.
 */
public enum QuestPartType
{
    WalkToPart(WalkToPart.class, "WalkToPart");

    private final Object obj;
    private final String name;

    QuestPartType(Object obj, String name)
    {
        this.obj = obj;
        this.name = name;
    }

    @Override
    public String toString()
    {
        return name;
    }

    public String getName()
    {
        return name;
    }

    public boolean equals(QuestPartType type)
    {
        return type.getName().equals(type.getName());
    }
}
