package com.simpsonwil.bukkit.blquests.parts;

import com.simpsonwil.bukkit.blquests.util.BLQUtils;
import com.simpsonwil.bukkit.blquests.exceptions.InvalidConfigFormat;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;

/**
 * Created by wil on 6/18/2015.
 */
public abstract class QuestPart
{
    protected final String partName;
    protected final FileConfiguration config;
    private boolean isFinished = false;

    private Player registeredPlayer;

    public QuestPart(File file) throws InvalidConfigFormat
    {
        checkForValidConfig(file);

        this.config = YamlConfiguration.loadConfiguration(file);
        this.partName = config.getString("name");
    }

    public void checkForValidConfig(File file) throws InvalidConfigFormat
    {
        BLQUtils.FormatUtil.checkFileFormat(file, "name", false);
    }

    protected void setRegisteredTo(Player player)
    {
        this.registeredPlayer = player;
    }

    public Player getRegisteredPlayer()
    {
        return registeredPlayer;
    }

    public final String getName()
    {
        return partName;
    }

    public final boolean isFinished()
    {
        return isFinished;
    }

    public void setFinished(boolean isFinished)
    {
        this.isFinished = isFinished;
    }

    public abstract void register(Player player);

    public abstract void unregister();
}
