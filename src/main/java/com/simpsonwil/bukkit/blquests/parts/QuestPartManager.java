package com.simpsonwil.bukkit.blquests.parts;

import com.simpsonwil.bukkit.blquests.util.BLQUtils;
import com.simpsonwil.bukkit.blquests.BLQuests;
import com.simpsonwil.bukkit.blquests.exceptions.InvalidConfigFormat;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wil on 6/20/2015.
 */
public class QuestPartManager
{
    //Singleton
    private QuestPartManager() { }
    private static final QuestPartManager instance = new QuestPartManager();
    public static final QuestPartManager getInstance()
    {
        return instance;
    }

    public List<QuestPart> parts;

    public List<File> invalidConfigs;

    public void setup()
    {
        parts = new ArrayList<>();
        invalidConfigs = new ArrayList<>();

        File[] files = BLQuests.PartsDirectoryFolder.listFiles();
        for(File file : getValidFiles(files))
        {
            try
            {
                FileConfiguration config = loadConfigIfValid(file);
                switch(config.getString("type").toLowerCase())
                {
                    case "walktopart":
                        parts.add(new WalkToPart(file));
                        break;

                    default:
                        throw new InvalidConfigFormat(file, "No valid type found.");
                }
            }
            catch (InvalidConfigFormat ex)
            {
                ex.printStackTrace();
            }
        }
    }

    public List<QuestPart> getParts()
    {
        return new ArrayList<>(parts);
    }

    public QuestPart getPart(String name)
    {
        for(QuestPart part : parts)
        {
            if(part.getName().equals(name))
            {
                return part;
            }
        }

        return null;
    }

    public List<File> getValidFiles(File[] files)
    {
        List<File> list = new ArrayList<>();
        for(File file : files)
        {
            if(file.isFile()
                    && BLQUtils.FormatUtil.isFileExtensionValid(file, "part", "yml"))
            {
                list.add(file);
            }
        }

        return list;
    }

    public FileConfiguration loadConfigIfValid(File file) throws InvalidConfigFormat
    {
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);

        BLQUtils.FormatUtil.checkFileFormat(file, "type", true);
        BLQUtils.FormatUtil.checkFileFormat(file, "name", true);

        return config;
    }

    public void reload()
    {

    }
}
