package com.simpsonwil.bukkit.blquests.parts;

import com.simpsonwil.bukkit.blquests.util.BLQUtils;
import com.simpsonwil.bukkit.blquests.BLQuests;
import com.simpsonwil.bukkit.blquests.events.PartCompletedEvent;
import com.simpsonwil.bukkit.blquests.exceptions.InvalidConfigFormat;
import com.strongholdmc.blcore.main.BlackLance;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;

/**
 * Created by wil on 6/18/2015.
 */
public class WalkToPart extends QuestPart implements Listener
{
    private final Location walkToLocation;
    private boolean isActive = false;

    private final WalkToPart instance;

    public WalkToPart(File file) throws InvalidConfigFormat
    {
        super(file);

        //Make the walk to location at a block location
        this.walkToLocation = BLQUtils.LocationUtil.stringToLocation(config.getString("location"));
        this.instance = this;
    }

    @Override
    public void register(Player player)
    {
        setRegisteredTo(player);
        Bukkit.getServer().getPluginManager().registerEvents(this, BLQuests.getInstance());
        isActive = true;
    }

    @Override
    public void unregister()
    {
        HandlerList.unregisterAll(this);
        isActive = false;
    }

    public boolean isActive()
    {
        return isActive;
    }

    @Override
    public void checkForValidConfig(File file) throws InvalidConfigFormat
    {
        super.checkForValidConfig(file);

        BLQUtils.FormatUtil.checkFileFormat(file, "location", false);
    }

    @EventHandler
    public void onWalk(final PlayerMoveEvent e)
    {
        new BukkitRunnable()
        {
            public void run()
            {
                if(isActive)
                {
                    Player p = e.getPlayer();
                    if (p.getName().equals(getRegisteredPlayer().getName()))
                    {
                        //Make the player just walk to the block location
                        if (e.getTo().getBlock().getLocation().equals(walkToLocation))
                        {
                            setFinished(true);
                            Bukkit.getPluginManager().callEvent(new PartCompletedEvent(instance, p));
                            unregister();
                        }
                    }
                }
            }
        }.runTaskAsynchronously(BlackLance.getPlugin());
    }


}
