package com.simpsonwil.bukkit.blquests;

import com.simpsonwil.bukkit.blquests.parts.QuestPart;
import com.simpsonwil.bukkit.blquests.requirements.QuestRequirement;
import com.simpsonwil.bukkit.blquests.rewards.QuestReward;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wil on 6/18/2015.
 */
public class Quest
{
    protected final String name;
    protected final List<QuestPart> parts;
    protected final List<QuestReward> rewards;
    protected final List<QuestRequirement> requirements;

    public Quest(String name, List<QuestPart> parts, List<QuestReward> rewards, List<QuestRequirement> requirements)
    {
        this.name = name;
        this.parts = parts;
        this.rewards = rewards;
        this.requirements = requirements;
    }

    public boolean isPartOfQuest(String partName)
    {
        for(QuestPart currentPart : parts)
        {
            if(currentPart.getName().equals(partName))
            {
                return true;
            }
        }

        return false;
    }

    public boolean isPartOfQuest(QuestPart part)
    {
        return isPartOfQuest(part.getName());
    }

    public List<QuestPart> getParts()
    {
        return new ArrayList<>(parts);
    }

    public List<QuestReward> getRewards()
    {
        return new ArrayList<>(rewards);
    }

    public List<QuestRequirement> getRequirements()
    {
        return new ArrayList<>(requirements);
    }

    public String getName()
    {
        return name;
    }

    public boolean isSimilar(Quest quest)
    {
        return quest.getName().equals(quest.getName());
    }
}
