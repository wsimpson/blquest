package com.simpsonwil.bukkit.blquests;

import com.simpsonwil.bukkit.blquests.commands.BLCommands;
import com.simpsonwil.bukkit.blquests.listeners.PartCompletedListener;
import com.simpsonwil.bukkit.blquests.listeners.QuestCompletedListener;
import com.simpsonwil.bukkit.blquests.parts.QuestPartManager;
import com.simpsonwil.bukkit.blquests.requirements.QuestRequirementManager;
import com.simpsonwil.bukkit.blquests.rewards.QuestRewardManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

/**
 * Created by wil on 6/17/2015.
 *
 * TODO: Add quest start
 *       Add commands
 *       Change to DB  instead of YML..?
 *       Add descriptions to all configs. If descriptions all null then the part won't be visible to the player?
 */
public class BLQuests extends JavaPlugin
{
    public static String PartsDirectoryPath;
    public static String QuestsDirectoryPath;
    public static String StartsDirectoryPath;
    public static String RewardsDirectoryPath;
    public static String RequirementsDirectoryPath;

    public static File PartsDirectoryFolder;
    public static File QuestsDirectoryFolder;
    public static File StartsDirectoryFolder;
    public static File RewardsDirectoryFolder;
    public static File RequirementsDirectoryFolder;

    private static BLQuests instance;

    public void onEnable()
    {
        //@TODO: REMOVE TILL END
        DBTemp.instance.setup(this);
        //@END

        instance = this;

        PartsDirectoryPath = getDataFolder()+"/parts";
        QuestsDirectoryPath = getDataFolder()+"/quests";
        StartsDirectoryPath = getDataFolder()+"/starts";
        RewardsDirectoryPath = getDataFolder()+"/rewards";
        RequirementsDirectoryPath = getDataFolder()+"/requirements";

        PartsDirectoryFolder = new File(PartsDirectoryPath);
        QuestsDirectoryFolder = new File(QuestsDirectoryPath);
        StartsDirectoryFolder = new File(StartsDirectoryPath);
        RewardsDirectoryFolder = new File(RewardsDirectoryPath);
        RequirementsDirectoryFolder = new File(RequirementsDirectoryPath);

        if(!PartsDirectoryFolder.exists()) PartsDirectoryFolder.mkdirs();
        if(!QuestsDirectoryFolder.exists()) QuestsDirectoryFolder.mkdirs();
        if(!StartsDirectoryFolder.exists()) StartsDirectoryFolder.mkdirs();
        if(!RewardsDirectoryFolder.exists()) RewardsDirectoryFolder.mkdirs();
        if(!RequirementsDirectoryFolder.exists()) RequirementsDirectoryFolder.mkdirs();

        QuestPartManager.getInstance().setup();
        QuestRewardManager.getInstance().setup();
        QuestRequirementManager.getInstance().setup();
        QuestManager.getInstance().setup();

        getServer().getPluginManager().registerEvents(new PartCompletedListener(), this);
        getServer().getPluginManager().registerEvents(new QuestCompletedListener(), this);

        getCommand("blq").setExecutor(new BLCommands(this));
        getCommand("blq").setExecutor(new BLCommands(this));
    }

    public static BLQuests getInstance()
    {
        return instance;
    }

    public static void main(String[] args)
    {
        System.out.print("test");
    }


    /**
     * Must be run on main thread or else shit breaks.
     */
    public synchronized void reloadManagers()
    {
        getServer().getScheduler().scheduleSyncDelayedTask(this, () ->
        {
            System.out.println("BLQuests: ### RELOAD INITIATED ###");


            System.out.println("BLQuests: Reloading QuestPartManager");
            QuestPartManager.getInstance().reload();
            System.out.println("BLQuests: Loaded QuestPartManager");

            System.out.println("BLQuests: Reloading QuestRewardManager");
            QuestRewardManager.getInstance().reload();
            System.out.println("BLQuests: Loaded QuestRewardManager");

            System.out.println("BLQuests: Reloading QuestRequirementManager");
            QuestRequirementManager.getInstance().reload();
            System.out.println("BLQuests: Loaded QuestRequirementManager");

            System.out.println("BLQuests: Reloading QuestManager");
            QuestManager.getInstance().reload();
            System.out.println("BLQuests: Loaded QuestManager");


            System.out.println("BLQuests: ### RELOAD COMPLETE ###");
        });
    }
}
