package com.simpsonwil.bukkit.blquests.events;

import com.simpsonwil.bukkit.blquests.parts.QuestPart;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by wil on 6/18/2015.
 */
public class PartCompletedEvent extends Event
{
    private static final HandlerList handlers = new HandlerList();

    private Player finisher;
    private QuestPart finishedPart;

    public PartCompletedEvent(QuestPart finishedPart, Player finisher)
    {
        this.finishedPart = finishedPart;
        this.finisher = finisher;
    }

    public Player getPlayer()
    {
        return finisher;
    }

    public QuestPart getQuestPart()
    {
        return finishedPart;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList()
    {
        return handlers;
    }
}
