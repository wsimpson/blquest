package com.simpsonwil.bukkit.blquests.events;

import com.simpsonwil.bukkit.blquests.Quest;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by wil on 6/18/2015.
 */
public class QuestCompletedEvent extends Event
{
    private static final HandlerList handlers = new HandlerList();

    private Player finisher;
    private Quest finishedQuest;

    public QuestCompletedEvent(Quest finishedQuest, Player finisher)
    {
        this.finishedQuest = finishedQuest;
        this.finisher = finisher;
    }

    public Player getPlayer()
    {
        return finisher;
    }

    public Quest getQuest()
    {
        return finishedQuest;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList()
    {
        return handlers;
    }
}
