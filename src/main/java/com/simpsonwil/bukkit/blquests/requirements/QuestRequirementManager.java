package com.simpsonwil.bukkit.blquests.requirements;

import com.simpsonwil.bukkit.blquests.util.BLQUtils;
import com.simpsonwil.bukkit.blquests.BLQuests;
import com.simpsonwil.bukkit.blquests.exceptions.InvalidConfigFormat;
import com.strongholdmc.blcore.playerclasses.RPGArmor;
import com.strongholdmc.blcore.playerclasses.RPGItems;
import com.strongholdmc.blcore.playerclasses.RPGWeapon;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wil on 6/20/2015.
 */
public class QuestRequirementManager
{
    //Singleton
    private QuestRequirementManager() { }
    private static final QuestRequirementManager instance = new QuestRequirementManager();
    public static final QuestRequirementManager getInstance()
    {
        return instance;
    }

    public List<QuestRequirement> requirements;

    public void setup()
    {
        this.requirements = new ArrayList<>();

        File[] files = BLQuests.RequirementsDirectoryFolder.listFiles();
        for(File file : getValidFiles(files))
        {
            try
            {
                FileConfiguration config = loadConfigIfValid(file);
                QuestRequirement questRequirement = new QuestRequirement(config.getString("name"));

                int level = 0;
                int money = 0;
                List<ItemStack> items = new ArrayList<>();

                if(config.contains("level")) level = config.getInt("level");
                if(config.contains("money")) money = config.getInt("money");
                if(config.contains("items"))
                {
                    for(String rawItemString : config.getStringList("items"))
                    {
                        String[] splitRawItemData = rawItemString.split("#");
                        Material material = Material.getMaterial(splitRawItemData[0]);
                        int amount = Integer.valueOf(splitRawItemData[1]);

                        if(material != Material.AIR
                                && material != null
                                && amount > 0)
                        {
                            ItemStack is = new ItemStack(material);
                            is.setAmount(amount);
                            items.add(is);
                        }
                    }
                }

                if(config.contains("rpg-items"))
                {
                    for(String rawWeaponString : config.getStringList("rpg-armor"))
                    {
                        String[] splitRawArmorData = rawWeaponString.split("#");
                        Material material = Material.getMaterial(splitRawArmorData[0]);
                        String serial = splitRawArmorData[1];

                        //Not sure if default for Material.getMaterial is AIR
                        if(RPGItems.hasArmor(serial)
                                && material != null
                                && material != Material.AIR)
                        {
                            ItemStack is = new ItemStack(material);
                            RPGArmor.addArmorLoreFromSerial(serial, is);
                            items.add(is);
                        }
                        else if(RPGItems.hasWeapon(serial)
                                && material != null
                                && material != Material.AIR)
                        {
                            ItemStack is = new ItemStack(material);
                            RPGWeapon.addWeaponLoreFromSerial(serial, is);
                            items.add(is);
                        }
                    }
                }

                questRequirement.setLevelRequirement(level);
                questRequirement.setMoneyRequirement(money);
                questRequirement.addItemRequirement(items);

                requirements.add(questRequirement);
            }
            catch (InvalidConfigFormat invalidConfigFormat)
            {
                invalidConfigFormat.printStackTrace();
            }
        }
    }

    public List<QuestRequirement> getRequirements()
    {
        return new ArrayList<>(requirements);
    }

    public QuestRequirement getRequirement(String name)
    {
        for(QuestRequirement requirement : requirements)
        {
            if(requirement.getName().equals(name))
            {
                return requirement;
            }
        }

        return null;
    }

    public List<File> getValidFiles(File[] files)
    {
        List<File> list = new ArrayList<>();
        for(File file : files)
        {
            if(file.isFile()
                    && BLQUtils.FormatUtil.isFileExtensionValid(file, "requirement", "yml"))
            {
                list.add(file);
            }
        }

        return list;
    }

    public FileConfiguration loadConfigIfValid(File file) throws InvalidConfigFormat
    {
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);

        BLQUtils.FormatUtil.checkFileFormat(file, "name", true);

        return config;
    }

    public void reload()
    {
        requirements.clear();
        setup();
    }
}
