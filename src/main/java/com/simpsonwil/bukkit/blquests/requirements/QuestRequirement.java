package com.simpsonwil.bukkit.blquests.requirements;

import com.strongholdmc.blcore.playerclasses.RPGArmor;
import com.strongholdmc.blcore.playerclasses.RPGItems;
import com.strongholdmc.blcore.playerclasses.RPGPlayer;
import com.strongholdmc.blcore.playerclasses.RPGWeapon;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wil on 6/21/2015.
 */
public class QuestRequirement
{
    private final String name;

    private int level = 0;
    private int money = 0;
    private Map<ItemStack, Integer> items = new HashMap<>();

    public QuestRequirement(String name)
    {
        this.name = name;
    }

    public void setLevelRequirement(int value)
    {
        this.level = value;
    }

    public void setMoneyRequirement(int value)
    {
        this.money = value;
    }

    public void addItemRequirement(List<ItemStack> items)
    {
        items.addAll(items);
    }

    public String getName()
    {
        return name;
    }

    public void addItemRequirement(ItemStack is, int amount)
    {
        items.put(is, amount);
    }

    public void addItemRequirement(Map<ItemStack, Integer> map)
    {
        items.putAll(map);
    }

    public boolean passesRequirement(RPGPlayer rp)
    {
        if(rp.getMoney() < money) return false;
        if(rp.getPlayer().getLevel() < level) return false;

        ItemStack[] playerInv = rp.getPlayer().getInventory().getContents();
        for(ItemStack is : items.keySet())
        {
            if(!hasEnough(is, items.get(is), playerInv)) return false;
        }

        return true;
    }

    private boolean hasSerial(String serial, ItemStack[] items)
    {
        for(ItemStack is : items)
        {
            if(RPGWeapon.isWeapon(is.getItemMeta().getLore()))
            {
                if(RPGWeapon.getSerialStatString(is).equals(serial))
                {
                    return true;
                }
            }
            else if(RPGArmor.isArmor(is.getItemMeta().getLore()))
            {
                if(RPGArmor.getSerialStatString(is).equals(serial))
                {
                    return true;
                }
            }
        }

        return false;
    }

    private boolean hasEnough(ItemStack is, int amount, ItemStack[] items)
    {
        boolean isRPGItem = RPGWeapon.isWeapon(is.getItemMeta().getLore()) || RPGArmor.isArmor(is.getItemMeta().getLore());
        int count = 0;
        for(ItemStack currentIS : items)
        {
            if(RPGWeapon.isWeapon(currentIS.getItemMeta().getLore()))
            {
                if(isRPGItem && RPGWeapon.getSerialStatString(currentIS).equals(RPGWeapon.getSerialStatString(is)))
                {
                    count += is.getAmount();
                }
            }
            else if(RPGArmor.isArmor(currentIS.getItemMeta().getLore()))
            {
                if(isRPGItem && RPGArmor.getSerialStatString(currentIS).equals(RPGArmor.getSerialStatString(is)))
                {
                    count += is.getAmount();
                }
            }
            else
            {
                if(!isRPGItem && is.getType().equals(is.getType()))
                {
                    count += is.getAmount();
                }
            }

            if(count >= amount)
            {
                return true;
            }
        }

        return false;
    }
}
