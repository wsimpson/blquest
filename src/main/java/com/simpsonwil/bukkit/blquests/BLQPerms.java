package com.simpsonwil.bukkit.blquests;

/**
 * Created by wil on 7/16/2015.
 */
public class BLQPerms
{
    public static final String BLQ_HELP = "blq.help";
    public static final String BLQ_RELOAD = "blq.help";
    public static final String BLQ_LIST_QUEST = "blq.list.quest";
    public static final String BLQ_LIST_PARTS = "blq.list.parts";
    public static final String BLQ_LIST_REQUIREMENTS = "blq.list.requirements";
    public static final String BLQ_LIST_REWARDS = "blq.list.rewards";
    public static final String BLQ_LIST_ALL = "blq.list.all";
}
