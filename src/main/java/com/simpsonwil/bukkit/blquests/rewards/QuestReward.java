package com.simpsonwil.bukkit.blquests.rewards;

import com.strongholdmc.blcore.playerclasses.RPGPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wil on 6/20/2015.
 */
public class QuestReward
{
    private final String name;
    private final int xp;
    private final int money;
    private final Map<ItemStack, Integer> items;

    public QuestReward(String name, int xp, int money, Map<ItemStack, Integer> items)
    {
        this.name = name;
        this.xp = xp;
        this.money = money;
        this.items = items;
    }

    public String getName()
    {
        return name;
    }

    public int getXP()
    {
        return xp;
    }

    public int getMoney()
    {
        return money;
    }

    public Map<ItemStack, Integer> getItems()
    {
        return items;
    }

    public void giveRewards(RPGPlayer rp)
    {
        specialReward(rp);
        rp.addXP(xp);
        rp.addMoney(money);

        //This is so you can give more than one "stack" of items to someone.
        Inventory inv = rp.getPlayer().getInventory();
        for(ItemStack is : items.keySet())
        {
            for(int i=0; i<items.get(is); i++)
            {
                inv.addItem(is);
            }
        }
    }

    //@TODO Account for stacking
    public boolean hasRoomForRewards(Player p)
    {
        Inventory inv = p.getInventory();
        int roomLeft = inv.getSize();
        for(int i=0; i<inv.getSize(); i++)
        {
            if(inv.getItem(i) != null)
            {
                roomLeft--;
            }
        }

        return roomLeft >= (items.size());
    }

    private void specialReward(RPGPlayer rp)
    {

    }
}
