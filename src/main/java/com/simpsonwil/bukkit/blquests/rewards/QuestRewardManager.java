package com.simpsonwil.bukkit.blquests.rewards;

import com.simpsonwil.bukkit.blquests.util.BLQUtils;
import com.simpsonwil.bukkit.blquests.BLQuests;
import com.simpsonwil.bukkit.blquests.exceptions.InvalidConfigFormat;
import com.strongholdmc.blcore.playerclasses.RPGArmor;
import com.strongholdmc.blcore.playerclasses.RPGItems;
import com.strongholdmc.blcore.playerclasses.RPGWeapon;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wil on 6/20/2015.
 */
public class QuestRewardManager
{
    //Singleton
    private QuestRewardManager() { }
    private static final QuestRewardManager instance = new QuestRewardManager();
    public static final QuestRewardManager getInstance()
    {
        return instance;
    }

    public List<QuestReward> rewards;

    //@TODO Do
    public void setup()
    {
        this.rewards = new ArrayList<>();

        File[] files = BLQuests.RewardsDirectoryFolder.listFiles();
        for(File file : files)
        {
            FileConfiguration config = null;
            try
            {
                config = loadConfigIfValid(file);
                String name = config.getString("name");
                int xp = 0;
                int money = 0;
                Map<ItemStack, Integer> items = new HashMap<>();

                if(config.contains("xp")) xp = config.getInt("xp");
                if(config.contains("money")) money = config.getInt("money");

                if(config.contains("items"))
                {
                    for(String rawItemString : config.getStringList("items"))
                    {
                        String[] splitRawItemData = rawItemString.split("#");
                        Material material = Material.getMaterial(splitRawItemData[0]);
                        int amount = Integer.valueOf(splitRawItemData[1]);

                        if(material != Material.AIR
                                && material != null
                                && amount > 0)
                        {
                            ItemStack is = new ItemStack(material);
                            items.put(is, amount);
                        }
                    }
                }

                if(config.contains("rpg-items"))
                {
                    for(String rawWeaponString : config.getStringList("rpg-armor"))
                    {
                        String[] splitRawRPGData = rawWeaponString.split("#");
                        Material material = Material.getMaterial(splitRawRPGData[0]);
                        String serial = splitRawRPGData[1];
                        int amount = Integer.valueOf(splitRawRPGData[2]);

                        //Not sure if default for Material.getMaterial is AIR
                        if(RPGItems.hasArmor(serial)
                                && material != null
                                && material != Material.AIR)
                        {
                            ItemStack is = new ItemStack(material);
                            RPGArmor.addArmorLoreFromSerial(serial, is);
                            items.put(is, amount);
                        }
                        else if(RPGItems.hasWeapon(serial)
                                && material != null
                                && material != Material.AIR)
                        {
                            ItemStack is = new ItemStack(material);
                            RPGWeapon.addWeaponLoreFromSerial(serial, is);
                            items.put(is, amount);
                        }
                    }
                }

                rewards.add(new QuestReward(name, xp, money, items));
            }
            catch (InvalidConfigFormat invalidConfigFormat)
            {
                invalidConfigFormat.printStackTrace();
            }
        }
    }

    public List<QuestReward> getRewards()
    {
        return new ArrayList<>(rewards);
    }

    public QuestReward getReward(String name)
    {
        for(QuestReward reward : rewards)
        {
            if(reward.getName().equals(name))
            {
                return reward;
            }
        }

        return null;
    }

    public List<File> getValidFiles(File[] files)
    {
        List<File> list = new ArrayList<>();
        for(File file : files)
        {
            if(file.isFile()
                    && BLQUtils.FormatUtil.isFileExtensionValid(file, "reward", "yml"))
            {
                list.add(file);
            }
        }

        return list;
    }

    public FileConfiguration loadConfigIfValid(File file) throws InvalidConfigFormat
    {
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);

        BLQUtils.FormatUtil.checkFileFormat(file, "name", true);

        return config;
    }

    public void reload()
    {
        rewards.clear();
        setup();
    }
}
