package com.simpsonwil.bukkit.blquests;

import com.simpsonwil.bukkit.blquests.events.QuestCompletedEvent;
import com.simpsonwil.bukkit.blquests.parts.QuestPart;
import com.simpsonwil.bukkit.blquests.requirements.QuestRequirement;
import com.simpsonwil.bukkit.blquests.rewards.QuestReward;
import com.strongholdmc.blcore.playerclasses.RPGPlayer;
import org.bukkit.Bukkit;

import java.util.List;

/**
 * Created by wil on 6/19/2015.
 */
public class ActiveQuest extends Quest
{
    private RPGPlayer activePlayer;
    private int partPosition;

    public ActiveQuest(Quest quest, RPGPlayer activePlayer, int questPartPosition)
    {
        super(quest.getName(), quest.getParts(), quest.getRewards(), quest.getRequirements());

        this.activePlayer = activePlayer;
        this.partPosition = questPartPosition;

        if(partPosition < 0 || partPosition >= parts.size())
        {
            partPosition = 0;
        }

        parts.get(partPosition).register(activePlayer.getPlayer());

    }

    public int getPartPosition()
    {
        return partPosition;
    }

    public QuestPart getCurrentPart()
    {
        return parts.get(partPosition);
    }

    public boolean setCurrentPosition(int position)
    {
        if(position < parts.size())
        {
            parts.get(partPosition).unregister();
            parts.get(position).register(activePlayer.getPlayer());

            partPosition = position;

            return true;
        }

        return false;
    }

    public synchronized void incrementQuestPart()
    {
        partPosition++;
        //Unregister part and do finish quest tasks
        if(isQuestFinished())
        {
            parts.get(partPosition-1).unregister();
            finishQuest();
        }
        //Register the new part and unregister old
        else
        {
            parts.get(partPosition-1).unregister();
            parts.get(partPosition).register(activePlayer.getPlayer());
        }
    }

    private void finishQuest()
    {
        Bukkit.getServer().getPluginManager().callEvent(new QuestCompletedEvent(this, activePlayer.getPlayer()));
    }

    public boolean isQuestFinished()
    {
        return partPosition >= parts.size();
    }
}
