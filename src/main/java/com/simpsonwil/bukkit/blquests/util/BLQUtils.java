package com.simpsonwil.bukkit.blquests.util;

import com.simpsonwil.bukkit.blquests.exceptions.InvalidConfigFormat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wil on 6/20/2015.
 */
public class BLQUtils
{
    public static final class LocationUtil
    {
        public static final Location stringToLocation(String string)
        {
            if (string.equals(null))
                return null;

            Location loc;
            String[] rawLocationData = string.split(":");

            try
            {
                World world = Bukkit.getWorld(rawLocationData[0]);
                double x = Double.valueOf(rawLocationData[1]);
                double y = Double.valueOf(rawLocationData[2]);
                double z = Double.valueOf(rawLocationData[3]);
                float yaw = Float.valueOf(rawLocationData[4]);
                float pitch = Float.valueOf(rawLocationData[5]);

                loc = new Location(world, x, y, z, yaw, pitch);

                return loc;

            }
            catch (Exception e)
            {
                System.out.println("BLQuests: The following string has become corrupt: "+ string);
                e.printStackTrace();
            }

            return null;
        }

        public static final String locationToString(Location location)
        {
            String string;

            String world = location.getWorld().getName();
            int x = location.getBlockX();
            int y = location.getBlockY();
            int z = location.getBlockZ();

            string = world + ":" + x + ":" + y + ":" + z + ":" + 0 + ":" + 0;

            return string;
        }

        public static String locationToExactString(Location loc)
        {
            String string;

            String world = loc.getWorld().getName();
            double x = loc.getX();
            double y = loc.getY();
            double z = loc.getZ();
            float yaw = loc.getYaw();
            float pitch = loc.getPitch();

            string = world + ":" + x + ":" + y + ":" + z + ":" + yaw + ":" + pitch;

            return string;
        }
    }

    public static final class FormatUtil
    {
        public static void checkFileFormat(File file, String section, boolean canBeNull) throws InvalidConfigFormat
        {
            FileConfiguration config = YamlConfiguration.loadConfiguration(file);
            if(!config.contains(section))
            {
                throw new InvalidConfigFormat(file, "Does not contain section \""+section+"\". ");
            }

            if(!canBeNull)
            {
                if(config.get(section) == null)
                {
                    throw new InvalidConfigFormat(file, "\""+section+"\" section cannot be null. ");
                }
            }
        }

        public static boolean isFileExtensionValid(File file, String... parts)
        {
            String[] fileParts = file.getName().split("\\.");
            int filePartsLength = fileParts.length;
            int partsLength = parts.length;

            if(filePartsLength <= partsLength) return false;

            for(int i=1, j=partsLength-1; j>=0; i++, j--)
            {
                if(!fileParts[filePartsLength-i].equals(parts[j]))
                {
                    return false;
                }
            }

            return true;
        }

        public static String[] truncateArgs(String[] args)
        {
            if(args.length-1 == 0) return new String[0];

            String[] newArgs = new String[args.length-1];

            for(int i=0; i<newArgs.length; i++)
            {
                newArgs[i] = args[i+1];
            }

            return newArgs;
        }

        public static String[] formatArgs(String[] args)
        {
            boolean isOpen = false;
            List<String> list = new ArrayList<>();
            for(String arg : args)
            {
                if(!isOpen)
                {
                    if(arg.startsWith("{{"))
                    {
                        arg = arg.substring(2);
                        isOpen = true;
                    }
                    list.add(arg);
                }
                else
                {
                    if(arg.endsWith("}}"))
                    {
                        arg = arg.substring(0, arg.length()-2);
                        isOpen = false;
                    }
                    list.set(list.size()-1, list.get(list.size()-1)+" "+arg);
                }
            }

            return list.toArray(new String[list.size()]);
        }
    }

    public static final class Validation
    {
        public static boolean canRunCommand(CommandSender sender, Enums.SenderType requiredSenderType, String requiredPermission)
        {
            if(requiredSenderType.isValid(sender))
            {
                if(sender.hasPermission(requiredPermission))
                {
                    return true;
                }
                else
                {
                    sender.sendMessage(ChatColor.RED+"You do not have the required permissions for this command.");
                }
            }

            return false;
        }
    }

    public static final class Enums
    {
        public enum SenderType
        {
            console("console"),
            player("player"),
            either("either");

            private String name;

            SenderType(String name)
            {
                this.name = name;
            }

            @Override
            public String toString()
            {
                return name;
            }

            public boolean isValid(CommandSender sender)
            {
                if(sender instanceof Player)
                {
                    if(toString().equals("console"))
                    {
                        sender.sendMessage(ChatColor.RED+"This command can only be run by a console.");
                        return false;
                    }

                    return true;
                }
                else
                {
                    if(toString().equals("player"))
                    {
                        sender.sendMessage(ChatColor.RED+"This command can only be run by a player.");
                        return false;
                    }

                    return true;
                }
            }
        }
    }
}
