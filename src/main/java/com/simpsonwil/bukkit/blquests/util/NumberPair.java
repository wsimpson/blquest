package com.simpsonwil.bukkit.blquests.util;

/**
 * Created by wil on 7/17/2015.
 */
public class NumberPair
{
    private Integer original;
    private Integer pair;

    public NumberPair(int x)
    {
        this.original = x;
    }

    public void setPair(int x)
    {
        this.pair = x;
    }

    public boolean isPaired()
    {
        return original != null && pair != null;
    }

    public int distance()
    {
        return Math.abs(original-pair);
    }
}
