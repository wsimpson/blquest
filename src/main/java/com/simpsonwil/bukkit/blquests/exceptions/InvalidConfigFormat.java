package com.simpsonwil.bukkit.blquests.exceptions;

import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;

/**
 * Created by wil on 6/20/2015.
 */
public class InvalidConfigFormat extends InvalidFormat
{
    public InvalidConfigFormat(File config, String message)
    {
        super(config.getName()+" has in invalid format: "+ message);
    }
}
