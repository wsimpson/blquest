package com.simpsonwil.bukkit.blquests.exceptions;

import org.bukkit.Bukkit;

import java.util.logging.Logger;

/**
 * Created by wil on 7/17/2015.
 */
public class InvalidFormat extends Exception
{
    private String message;

    public InvalidFormat(String message)
    {
        super(message);

        this.message = message;
    }

    @Override
    public void printStackTrace()
    {

        Bukkit.getLogger().severe(
                "\n##################\n"+
                "[BLQuests] "+message+
                "\n##################"
        );
    }
}
