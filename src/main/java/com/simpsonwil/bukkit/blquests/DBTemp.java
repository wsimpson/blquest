package com.simpsonwil.bukkit.blquests;

import com.strongholdmc.blcore.playerclasses.RPGPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;

/**
 * Created by wil on 7/14/2015.
 */
public class DBTemp
{
    private Plugin plugin;
    private String user = "root";
    private String database = "rpg";
    private String password = "enter11284";
    private String port = "3306";
    private String hostname = "localhost";

    private Connection connection;

    private String tableName = "blq_info";
    private String idCol = "id";
    private String uuidCol = "uuid";
    private String finishedCol = "finished";
    private String activeCol = "active";

    private String mainSplitter = "|||";
    private String activeSmallSplitter = "#";

    //Singleton
    private DBTemp() { }
    public static final DBTemp instance = new DBTemp();

    public void setup(Plugin plugin)
    {
        this.plugin = plugin;;
        this.connection = openConnection();
    }

    public Connection openConnection()
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://"
                            + this.hostname + ":" + this.port + "/" + this.database,
                    this.user, this.password);
        }
        catch (SQLException e)
        {
            plugin.getLogger().log(
                    Level.SEVERE,
                    "Could not connect to MySQL server! because: "
                            + e.getMessage());
        }
        catch (ClassNotFoundException e)
        {
            plugin.getLogger().log(Level.SEVERE, "JDBC Driver not found!");
        }

        return connection;
    }

    public boolean checkConnection()
    {
        return connection != null;
    }

    public Connection getConnection()
    {
        return connection;
    }

    public void createTables()
    {
        try {
            connection.createStatement().executeUpdate("CREATE TABLE"+tableName+
                                                       "("+
                                                        idCol+"int PRIMARY KEY NOT NULL,"+
                                                        uuidCol+"varchar(255) NOT NULL,"+
                                                        finishedCol+"varchar(255) NULLABLE,"+
                                                        activeCol+"varchar(255) NULLABLE"+
                                                       ");");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Map<String, Integer> getActiveQuestsFromData(String fullRawData)
    {
        Map map = new HashMap<>();

        String[] individualRawData = fullRawData.split(mainSplitter);
        for(String dataPiece : individualRawData)
        {
            String[] rawData = dataPiece.split(activeSmallSplitter);
            if(rawData.length == 2)
            {
                String questName = rawData[0];
                String questPos = rawData[1];

                map.put(questName, Integer.valueOf(questPos));
            }
            else
            {
                //TODO: Figure out how to deal with malformed SQL data
            }
        }

        return map;
    }

    private List<String> getFinishedQuestsFromData(String fullRawData)
    {
        List list = new ArrayList<>();

        String[] individualRawData = fullRawData.split(mainSplitter);
        for(String rawData : individualRawData)
        {
            list.add(rawData);
        }

        return list;
    }

    private String getDataStringFromActiveQuests(List<ActiveQuest> activeQuests)
    {
        /*
        String string = "";

        for(int i=0; i<activeQuests.size(); i++)
        {
            String name = activeQuests.get(i).getName();
            String pos = activeQuests.get(i).getPartPosition()+"";
            string += name+activeSmallSplitter+pos;

            if(i != activeQuests.size()-1)
            {
                string += mainSplitter;
            }
        }

        return string;
        */

        Map<String, Integer> activeQuestList = new HashMap<>();
        for(ActiveQuest activeQuest : activeQuests)
        {
            activeQuestList.put(activeQuest.getName(), activeQuest.getPartPosition());
        }

        return getDataStringFromActiveQuests(activeQuestList);
    }

    private String getDataStringFromActiveQuests(Map<String, Integer> activeQuests)
    {
        String string = "";

        String[] keys = activeQuests.keySet().toArray(new String[activeQuests.keySet().size()]);

        for(int i=0; i<activeQuests.size(); i++)
        {
            String name = keys[i];
            String pos = activeQuests.get(keys[i])+"";
            string += name+activeSmallSplitter+pos;

            if(i != activeQuests.size()-1)
            {
                string += mainSplitter;
            }
        }

        return string;
    }

    private String getDataStringFromFinishedQuests(List<String> finishedQuests)
    {
        String string = "";

        for(int i=0; i<finishedQuests.size(); i++)
        {
            String name = finishedQuests.get(i);
            string += name+activeSmallSplitter;

            if(i != finishedQuests.size()-1)
            {
                string += mainSplitter;
            }
        }

        return string;
    }

    public Map<String, Integer> getActiveQuests(String uuid)
    {
        Map map = new HashMap<>();
        try
        {
            ResultSet rs = connection.createStatement().executeQuery("SELECT "+activeCol+" FROM "+tableName+" WHERE '"+uuidCol+"'='"+uuid+"';");

            if(rs != null)
            {
                String fullRawData = rs.getString(finishedCol);
                map = getActiveQuestsFromData(fullRawData);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return map;
    }

    public void setActiveQuests(UUID uuid, Map<String, Integer> map)
    {
        String data = getDataStringFromActiveQuests(map);

        try
        {
            connection.createStatement().executeUpdate("UPDATE TABLE "+tableName+" SET '"+activeCol+"'='"+data+"' WHERE '"+uuidCol+"'='"+uuid+"';");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public List<String> getFinishedQuests(String uuid)
    {
        List<String> list = new ArrayList<>();

        try
        {
            ResultSet rs = connection.createStatement().executeQuery("SELECT "+finishedCol+" FROM "+tableName+" WHERE '"+uuidCol+"'='"+uuid+"';");

            if(rs != null)
            {
                String fullRawData = rs.getString(finishedCol);
                list = getFinishedQuestsFromData(fullRawData);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return list;
    }

    public List<String> getFinishedQuests(UUID uuid)
    {
        return getFinishedQuests(uuid.toString());
    }

    public List<String> getFinishedQuests(Player p)
    {
        return getFinishedQuests(p.getUniqueId());
    }

    public List<String> getFinishedQuests(RPGPlayer rpgPlayer)
    {
        return getFinishedQuests(rpgPlayer.getPlayer());
    }

    public void setFinishedQuests(UUID uuid, List<String> list)
    {
        String data = getDataStringFromFinishedQuests(list);

        try
        {
            connection.createStatement().executeUpdate("UPDATE TABLE "+tableName+" SET '"+finishedCol+"'='"+data+"' WHERE '"+uuidCol+"'='"+uuid+"';");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void removeFinishedQuest(UUID uuid, String quest)
    {
        try
        {
            ResultSet rs = connection.createStatement().executeQuery("SELECT "+finishedCol+" FROM "+tableName+" WHERE '"+uuidCol+"'='"+uuid+"';");

            if(rs != null)
            {
                String fullRawData = rs.getString(finishedCol);

                List<String> list = getFinishedQuestsFromData(fullRawData);
                list.remove(quest);

                setFinishedQuests(uuid, list);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void addFinishedQuest(UUID uuid, String quest)
    {
        try
        {
            ResultSet rs = connection.createStatement().executeQuery("SELECT "+finishedCol+" FROM "+tableName+" WHERE '"+uuidCol+"'='"+uuid+"';");

            if(rs != null)
            {
                String fullRawData = rs.getString(finishedCol);

                List<String> list = getFinishedQuestsFromData(fullRawData);
                if(!list.contains(quest)) list.add(quest);

                setFinishedQuests(uuid, list);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void removeActiveQuest(UUID uuid, String quest)
    {
        try
        {
            ResultSet rs = connection.createStatement().executeQuery("SELECT "+activeCol+" FROM "+tableName+" WHERE '"+uuidCol+"'='"+uuid+"';");

            if(rs != null)
            {
                String fullRawData = rs.getString(finishedCol);

                Map<String, Integer> map = getActiveQuestsFromData(fullRawData);
                map.remove(quest);

                setActiveQuests(uuid, map);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void addActiveQuest(UUID uuid, String quest, int position)
    {
        try
        {
            ResultSet rs = connection.createStatement().executeQuery("SELECT "+activeCol+" FROM "+tableName+" WHERE '"+uuidCol+"'='"+uuid+"';");

            if(rs != null)
            {
                String fullRawData = rs.getString(finishedCol);

                Map<String, Integer> map = getActiveQuestsFromData(fullRawData);
                map.put(quest, position);

                setActiveQuests(uuid, map);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
}
