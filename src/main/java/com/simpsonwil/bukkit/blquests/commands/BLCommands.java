package com.simpsonwil.bukkit.blquests.commands;

import com.simpsonwil.bukkit.blquests.*;
import com.simpsonwil.bukkit.blquests.parts.QuestPart;
import com.simpsonwil.bukkit.blquests.parts.QuestPartManager;
import com.simpsonwil.bukkit.blquests.requirements.QuestRequirement;
import com.simpsonwil.bukkit.blquests.requirements.QuestRequirementManager;
import com.simpsonwil.bukkit.blquests.rewards.QuestReward;
import com.simpsonwil.bukkit.blquests.rewards.QuestRewardManager;
import com.simpsonwil.bukkit.blquests.util.BLQUtils;
import com.strongholdmc.blcore.playerclasses.RPGPlayer;
import com.strongholdmc.blcore.storage.RPGPlayers;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Iterator;


/**
 * Created by wil on 7/16/2015.
 *
 * @TODO: Add all permissions checking with BLQUtils.Valdiation.canRunCommand
 */
public class BLCommands implements CommandExecutor
{
    private BLQuests plugin;
    public BLCommands(BLQuests plugin)
    {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        String command = cmd.getName();
        if(command.equalsIgnoreCase("blq"))
        {
            if(BLQUtils.Validation.canRunCommand(sender, BLQUtils.Enums.SenderType.either, BLQPerms.BLQ_HELP))
            {
                args = BLQUtils.FormatUtil.formatArgs(args);
                return blq(sender, args);
            }
        }

        return true;
    }

    public boolean blq(CommandSender sender, String[] args)
    {
        if(args.length >= 1)
        {
            String arg = args[0];
            switch(arg.toLowerCase())
            {
                case "reload":
                    return blq_reload(sender, BLQUtils.FormatUtil.truncateArgs(args));

                case "list":
                    return blq_list(sender, BLQUtils.FormatUtil.truncateArgs(args));

                case "edit":
                    return blq_edit(sender, BLQUtils.FormatUtil.truncateArgs(args));
            }
        }

        sender.sendMessage(ChatColor.AQUA+"/blq reload"+ChatColor.GRAY+" - Reload the blq plugin configs");
        sender.sendMessage(ChatColor.AQUA+"/blq list"+ChatColor.GRAY+" - Use for more information");
        sender.sendMessage(ChatColor.AQUA+"/blq edit"+ChatColor.GRAY+" - Use for more information");
        sender.sendMessage(ChatColor.AQUA+"/blq info"+ChatColor.GRAY+" - Use for more information");
        sender.sendMessage(ChatColor.GRAY+"REMEMBER: You must put quests inside of {{ }}. EX: {{Best Quest Ever}}");
        return true;
    }

    public boolean blq_reload(CommandSender sender, String[] args)
    {
        plugin.reloadManagers();
        return true;
    }

    public boolean blq_edit(CommandSender sender, String[] args)
    {
        if(args.length >= 1)
        {
            String playerName = args[0];
            Player target = plugin.getServer().getPlayer(playerName);
            if(target != null)
            {
                return blq_edit_player(sender, BLQUtils.FormatUtil.truncateArgs(args), RPGPlayers.getFullMap().get(target.getUniqueId()));
            }
            else
            {
                sender.sendMessage(ChatColor.RED+"There is no player: "+playerName);
                return true;
            }
        }

        sender.sendMessage(ChatColor.AQUA+"/blq edit <player>"+ChatColor.GRAY+" - Type for more information");
        return true;
    }

    public boolean blq_edit_player(CommandSender sender, String[] args, RPGPlayer player)
    {
        String name = player.getPlayer().getName();

        if(args.length == 1)
        {
            String arg = args[0];
            switch(arg.toLowerCase())
            {
                case "addactive":
                    sender.sendMessage(ChatColor.AQUA+"/blq edit "+name+" addActive <quest name>"+ChatColor.GRAY+" - Add an active quest to a player");
                    return true;

                case "remactive":
                    sender.sendMessage(ChatColor.AQUA+"/blq edit "+name+" remActive <quest name>"+ChatColor.GRAY+" - Remove an active quest from a player");
                    return true;

                case "setactive":
                    sender.sendMessage(ChatColor.AQUA+"/blq edit "+name+" setActive <quest name> <position>"+ChatColor.GRAY+" - Set a player to a position in a quest.");
                    return true;

                case "addfinished":
                    sender.sendMessage(ChatColor.AQUA+"/blq edit "+name+" addFinished <quest name>"+ChatColor.GRAY+" - Add a finished quests to the player");
                    return true;

                case "remfinished":
                    sender.sendMessage(ChatColor.AQUA+"/blq edit "+name+" remFinished <quest name>"+ChatColor.GRAY+" - Remove a finished quests to the player");
                    return true;
            }
        }
        else if(args.length >= 2)
        {
            String type = args[0];
            String questName = args[1];
            QuestManager manager = QuestManager.getInstance();
            if(manager.isQuest(questName))
            {
                Quest quest = manager.getQuestFromName(questName);
                switch(type.toLowerCase())
                {
                    case "addactive":
                        return blq_edit_player_addActive(sender, BLQUtils.FormatUtil.truncateArgs(args), player, quest);

                    case "remactive":
                        return blq_edit_player_remActive(sender, BLQUtils.FormatUtil.truncateArgs(args), player, quest);

                    case "setactive":
                        return blq_edit_player_setActive(sender, BLQUtils.FormatUtil.truncateArgs(args), player, quest);

                    case "addfinished":
                        return blq_edit_player_addFinished(sender, BLQUtils.FormatUtil.truncateArgs(args), player, quest);

                    case "remfinished":
                        return blq_edit_player_remFinished(sender, BLQUtils.FormatUtil.truncateArgs(args), player, quest);
                }
            }
            else
            {
                sender.sendMessage(ChatColor.RED+"ERROR: There is no quest: "+questName);

                return true;
            }
        }

        sender.sendMessage(ChatColor.AQUA+"/blq edit "+name+" addActive"+ChatColor.GRAY+" - Type for more information");
        sender.sendMessage(ChatColor.AQUA+"/blq edit "+name+" remActive"+ChatColor.GRAY+" - Type for more information");
        sender.sendMessage(ChatColor.AQUA+"/blq edit "+name+" setActive"+ChatColor.GRAY+" - Type for more information");
        sender.sendMessage(ChatColor.AQUA+"/blq edit "+name+" addFinished"+ChatColor.GRAY+" - Type for more information");
        sender.sendMessage(ChatColor.AQUA+"/blq edit "+name+" remFinished"+ChatColor.GRAY+" - Type for more information");
        return true;
    }

    public boolean blq_edit_player_addActive(CommandSender sender, String[] args, RPGPlayer player, Quest quest)
    {
        ActiveQuest activeQuest = QuestManager.getInstance().activateNewQuest(quest, player);
        if(activeQuest != null)
        {
            sender.sendMessage(ChatColor.GREEN+player.getPlayer().getName()+" now has the active quest: "+activeQuest.getName());
        }
        else
        {
            sender.sendMessage(ChatColor.RED+"ERROR: Player already has this quest active.");
        }

        return true;
    }

    public boolean blq_edit_player_remActive(CommandSender sender, String[] args, RPGPlayer player, Quest quest)
    {
        boolean wasRemoved = false;

        Iterator<ActiveQuest> iterator = player.getActiveQuests().iterator();
        while(iterator.hasNext())
        {
            ActiveQuest activeQuest = iterator.next();
            if(activeQuest.getName().equalsIgnoreCase(quest.getName()))
            {
                iterator.remove();
                wasRemoved = true;
            }
        }

        if(wasRemoved)
        {
            sender.sendMessage(ChatColor.GREEN+player.getPlayer().getName()+" no longer has the active quest: "+quest.getName());
        }
        else
        {
            sender.sendMessage(ChatColor.RED+player.getPlayer().getName()+" didn't have the active quest: "+quest.getName());
        }

        return true;
    }

    public boolean blq_edit_player_setActive(CommandSender sender, String[] args, RPGPlayer player, Quest quest)
    {
        String name = player.getPlayer().getName();
        if(args.length >= 1)
        {
            int position = Integer.valueOf(args[0]);
            if(player.isActiveQuest(quest))
            {
                player.getActiveQuests().stream().filter(currentActiveQuest -> currentActiveQuest.getName().equalsIgnoreCase(quest.getName())).forEach(currentActiveQuest ->
                {
                    if(currentActiveQuest.setCurrentPosition(position))
                    {
                        sender.sendMessage(ChatColor.GREEN + name + " now has the active quest: " + currentActiveQuest.getName() + " at position " + position);
                    }
                    else
                    {
                        sender.sendMessage(ChatColor.RED+"ERROR: That position is higher than the number of total positions.");
                    }
                });
            }
        }

        sender.sendMessage(ChatColor.AQUA+"/blq edit "+name+" setActive <quest name> <position>"+ChatColor.GRAY+" - Set a player to a position in a quest.");
        return true;
    }

    public boolean blq_edit_player_addFinished(CommandSender sender, String[] args, RPGPlayer player, Quest quest)
    {
        if(QuestManager.getInstance().finishQuest(quest, player))
        {
            sender.sendMessage(ChatColor.GREEN+player.getPlayer().getName()+" now has the quest: "+quest.getName()+" finished.");
        }
        else
        {
            sender.sendMessage(ChatColor.RED+"ERROR: "+player.getPlayer().getName()+" already has the quest: "+quest.getName()+" finished.");
        }

        return true;
    }

    public boolean blq_edit_player_remFinished(CommandSender sender, String[] args, RPGPlayer player, Quest quest)
    {
        //Probably should move this to blCore but I'm to lazy right now.
        if(player.isQuestFinished(quest))
        {
            Iterator<String> iterator = player.getFinishedQuests().iterator();
            while(iterator.hasNext())
            {
                String finishedQuest = iterator.next();
                if(finishedQuest.equalsIgnoreCase(quest.getName()))
                {
                    iterator.remove();
                }
            }

            sender.sendMessage(ChatColor.GREEN+player.getPlayer().getName()+" no longer has the quest: "+quest.getName()+" finished.");
        }
        else
        {
            sender.sendMessage(ChatColor.RED+"ERROR: "+player.getPlayer().getName()+" doesn't has the quest: "+quest.getName()+" finished.");
        }
        return true;
    }

    public boolean blq_list(CommandSender sender, String[] args)
    {
        if(args.length >= 1)
        {
            String type = args[0];
            String[] newArgs = BLQUtils.FormatUtil.truncateArgs(args);
            if(type.equalsIgnoreCase("quests"))
            {
                return blq_list_quests(sender, newArgs);
            }
            else if(type.equalsIgnoreCase("parts"))
            {
                return blq_list_parts(sender, newArgs);
            }
            else if(type.equalsIgnoreCase("rewards"))
            {
                return blq_list_rewards(sender, newArgs);
            }
            else if(type.equalsIgnoreCase("requirements"))
            {
                return blq_list_requirements(sender, newArgs);
            }
            else if(type.equalsIgnoreCase("all"))
            {
                return blq_list_quests(sender, newArgs)
                        && blq_list_parts(sender, newArgs)
                        && blq_list_rewards(sender, newArgs)
                        && blq_list_requirements(sender, newArgs);
            }
        }

        sender.sendMessage(ChatColor.AQUA+"/blq list quests"+ChatColor.GRAY+" - List all of the available quests");
        sender.sendMessage(ChatColor.AQUA+"/blq list parts"+ChatColor.GRAY+" - List all of the available parts");
        sender.sendMessage(ChatColor.AQUA+"/blq list rewards"+ChatColor.GRAY+" - List all of the available rewards");
        sender.sendMessage(ChatColor.AQUA+"/blq list requirements"+ChatColor.GRAY+" - List all of the available requirements");
        sender.sendMessage(ChatColor.AQUA+"/blq list all"+ChatColor.GRAY+" - List all available quests, parts, rewards, and requirements");
        return true;
    }

    public boolean blq_list_quests(CommandSender sender, String[] args)
    {
        sender.sendMessage(ChatColor.DARK_GRAY+"### QUESTS ###");
        for(Quest quest : QuestManager.getInstance().getQuests())
        {
            sender.sendMessage(ChatColor.AQUA + quest.getName());
        }
        return true;
    }

    public boolean blq_list_parts(CommandSender sender, String[] args)
    {
        sender.sendMessage(ChatColor.DARK_GRAY+"### PARTS ###");
        for(QuestPart part : QuestPartManager.getInstance().getParts())
        {
            sender.sendMessage(ChatColor.AQUA + part.getName());
        }
        return true;
    }

    public boolean blq_list_rewards(CommandSender sender, String[] args)
    {
        sender.sendMessage(ChatColor.DARK_GRAY+"### REWARDS ###");
        for(QuestReward reward : QuestRewardManager.getInstance().getRewards())
        {
            sender.sendMessage(ChatColor.AQUA + reward.getName());
        }
        return true;
    }

    public boolean blq_list_requirements(CommandSender sender, String[] args)
    {
        sender.sendMessage(ChatColor.DARK_GRAY+"### REQUIREMENTS ###");
        for(QuestRequirement requirement : QuestRequirementManager.getInstance().getRequirements())
        {
            sender.sendMessage(ChatColor.AQUA + requirement.getName());
        }
        return true;
    }
}
