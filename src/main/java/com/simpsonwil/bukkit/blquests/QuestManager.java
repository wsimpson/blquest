package com.simpsonwil.bukkit.blquests;

import com.simpsonwil.bukkit.blquests.exceptions.InvalidConfigFormat;
import com.simpsonwil.bukkit.blquests.parts.QuestPart;
import com.simpsonwil.bukkit.blquests.parts.QuestPartManager;
import com.simpsonwil.bukkit.blquests.requirements.QuestRequirement;
import com.simpsonwil.bukkit.blquests.requirements.QuestRequirementManager;
import com.simpsonwil.bukkit.blquests.rewards.QuestReward;
import com.simpsonwil.bukkit.blquests.rewards.QuestRewardManager;
import com.simpsonwil.bukkit.blquests.util.BLQUtils;
import com.strongholdmc.blcore.playerclasses.RPGPlayer;
import com.strongholdmc.blcore.storage.RPGPlayers;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by wil on 6/19/2015.
 */
public final class QuestManager
{
    //Singleton
    private QuestManager() { }
    private static final QuestManager instance = new QuestManager();
    public static final QuestManager getInstance()
    {
        return instance;
    }

    private List<Quest> quests;

    public void setup()
    {
        this.quests = new ArrayList<>();
        setupQuests();
        setupPlayers();
    }

    private void setupQuests()
    {
        File[] files = BLQuests.QuestsDirectoryFolder.listFiles();
        for(File file : getValidFiles(files))
        {
            try
            {
                FileConfiguration config = loadConfigIfValid(file);
                if(config != null)
                {
                    String name = config.getString("name");
                    List<QuestPart> parts = new ArrayList<>();
                    List<QuestRequirement> requirements = new ArrayList<>();
                    List<QuestReward> rewards = new ArrayList<>();
                    for(String partName : config.getStringList("parts"))
                    {
                        try
                        {
                            QuestPart part = QuestPartManager.getInstance().getPart(partName);

                            if(part == null) throw new InvalidConfigFormat(file, "There is no part: "+partName);

                            parts.add(part);
                        }
                        catch (InvalidConfigFormat ex)
                        {
                            ex.printStackTrace();
                        }
                    }

                    for(String requirementName : config.getStringList("requirements"))
                    {
                        try
                        {
                            QuestRequirement requirement = QuestRequirementManager.getInstance().getRequirement(requirementName);

                            if(requirement == null) throw new InvalidConfigFormat(file, "There is no requirement: "+requirementName);

                            requirements.add(requirement);
                        }
                        catch (InvalidConfigFormat ex)
                        {
                            ex.printStackTrace();
                        }
                    }

                    for(String rewardName : config.getStringList("rewards"))
                    {
                        try
                        {
                            QuestReward reward = QuestRewardManager.getInstance().getReward(rewardName);

                            if(reward == null) throw new InvalidConfigFormat(file, "There is no reward: "+rewardName);

                            rewards.add(reward);
                        }
                        catch (InvalidConfigFormat ex)
                        {
                            ex.printStackTrace();
                        }
                    }

                    quests.add(new Quest(name, parts, rewards, requirements));
                }
            }
            catch (InvalidConfigFormat invalidConfigFormat)
            {
                invalidConfigFormat.printStackTrace();
            }
        }
    }

    public List<File> getValidFiles(File[] files)
    {
        List<File> list = new ArrayList<>();
        for(File file : files)
        {
            if(file.isFile()
                    && BLQUtils.FormatUtil.isFileExtensionValid(file, "quest", "yml"))
            {
                list.add(file);
            }
        }

        return list;
    }

    private void setupPlayers()
    {
        for(UUID uuid : RPGPlayers.getFullMap().keySet())
        {
            RPGPlayer rp = RPGPlayers.getRPGPlayer(Bukkit.getPlayer(uuid));

            List<String> finishedQuestNames = DBTemp.instance.getFinishedQuests(uuid);
            Map<String, Integer> activeQuestNamesAndPostion = DBTemp.instance.getActiveQuests(uuid.toString());

            finishedQuestNames.forEach(rp::addFinishedQuest);

            for(String questName : activeQuestNamesAndPostion.keySet())
            {
                rp.addActiveQuest(activateQuest(getQuestFromName(questName), rp, activeQuestNamesAndPostion.get(questName)));
            }
        }
    }

    public Quest getQuestFromName(String name)
    {
        for(Quest quest : quests)
        {
            if(quest.getName().equalsIgnoreCase(name))
            {
                return quest;
            }
        }

        return null;
    }

    public List<Quest> getQuestsFromNameList(List<String> questNames)
    {
        List<Quest> quests = new ArrayList<Quest>();

        for(String questName : questNames)
        {
            Quest quest = getQuestFromName(questName);
            if(quest != null) quests.add(quest);
        }

        return quests;
    }

    public ActiveQuest activateNewQuest(Quest quest, RPGPlayer rp)
    {
        return new ActiveQuest(quest, rp, 0);
    }

    public ActiveQuest activateQuest(Quest quest, RPGPlayer rp, int position)
    {
        if(!rp.isActiveQuest(quest))
        {
            ActiveQuest activeQuest = new ActiveQuest(quest, rp, position);
            DBTemp.instance.addActiveQuest(rp.getPlayer().getUniqueId(), quest.getName(), position);
            rp.addActiveQuest(activeQuest);

            return activeQuest;
        }

        return null;
    }

    public boolean finishQuest(Quest quest, RPGPlayer player)
    {
        if(!player.isQuestFinished(quest))
        {
            DBTemp.instance.addFinishedQuest(player.getPlayer().getUniqueId(), quest.getName());
            player.getFinishedQuests().add(quest.getName());
        }

        return false;
    }

    private FileConfiguration loadConfigIfValid(File file) throws InvalidConfigFormat
    {
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);

        BLQUtils.FormatUtil.checkFileFormat(file, "name", true);
        BLQUtils.FormatUtil.checkFileFormat(file, "requirements", true);
        BLQUtils.FormatUtil.checkFileFormat(file, "questStart", true);
        BLQUtils.FormatUtil.checkFileFormat(file, "rewards", true);
        BLQUtils.FormatUtil.checkFileFormat(file, "parts", true);

        return config;
    }

    public boolean isQuest(String name)
    {
        for(Quest quest : quests)
        {
            if(quest.getName().equalsIgnoreCase(name))
            {
                return true;
            }
        }

        return false;
    }

    public List<Quest> getQuests()
    {
        return new ArrayList<>(quests);
    }

    public void reload()
    {
        quests.clear();
        setup();
    }
}
